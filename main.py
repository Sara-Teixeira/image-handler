import os

from PIL import Image


class ImageHandler:
    def is_image(self, file_name):
        if file_name.endswith('png') or file_name.endswith('jpeg'):
            return True
        return False

    def reduce_size_images(self, input_dir, output_dir, width, height, ext='.jpg'):
        files = [name for name in os.listdir(input_dir) if self.is_image(name)]
        for name in files:
            image = Image.open(os.path.join(input_dir, name)).convert('RGB')
            resized = image.resize((width, height))
            name_without_extension = os.path.splitext(name)[0]
            resized.save(os.path.join(output_dir, name_without_extension + ext))


if __name__ == "__main__":
    directory = '/home/sara/Imagens/teste/'

    image_handler = ImageHandler()
    image_handler.reduce_size_images(directory, directory, width=500, height=500)
